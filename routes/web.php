<?php

use Illuminate\Support\Facades\Route;
use News\Http\Controllers\CategoryController;
use News\Http\Controllers\NewsController;
use News\Http\Controllers\SettingsController;
use News\Models\Settings;



Route::middleware('web')->group(function (){
    Route::middleware('auth_pacage')->group(function () {
        Route::group(['prefix'=>"admin", 'as'=>'admin.', 'middleware' => 'is_admin'],static function(){
            Route::resource('news', NewsController::class);
            Route::get('news/lock/{news}',[NewsController::class,'lock'])->name('lock');
            Route::get('news/unlock/{news}',[NewsController::class,'unlock'])->name('unlock');
            Route::group(['prefix' => 'news', 'as' => 'news.'], static function(){
                Route::post('remove_image/{news}',[NewsController::class,'removeImage']);
                Route::resource('category', CategoryController::class);
                Route::resource('settings', SettingsController::class);
            });
        });
    });
    try {
        $settings = Settings::find(1);
        Route::get('/'.$settings->url,[NewsController::class,'frontIndex'])->name('news');
        Route::get('/'.$settings->url.'/category/{url}',[NewsController::class,'frontCategory'])->name('news.category');
        Route::get('/'.$settings->url . '/{url}',[NewsController::class,'news'])->name('news.news');
    } catch (Exception $exception) {
        //throw $th;
    }
});
