<?php

namespace News\Requests;


use Carbon\Carbon;
use Catalog\Models\Product;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use News\Models\News;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    public function all($keys = null)
    {
        $data = parent::all($keys);

        $images = $data['images'];

        $jsonImages = json_encode($images);

        $data['images'] = $jsonImages;

        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'id' => ['required'],
            'category_id' => ['required', 'integer'],
            'category_id.*' => ['exists:categories_news,id'], // проверяет каждый элемент с таблицей categories_news c полем id
            'title' => ['required', 'min:2', 'max:200'],
            'content' => ['required', 'min:2'],
            'description' => ['required'],
            'url' => ['required', Rule::unique(News::class)->ignore($this->id, 'id')],
            'created_at' => ['nullable'],
            'images' => ['sometimes'],
            'seoKeywords' => ['nullable', 'string'],
            'seoTitle' => ['nullable','string'],
            'seoDescription' => ['nullable', 'string'],
        ];
    }

    public function getCategoriesIds(): array
    {
        return (array) $this->validated('category_id');
    }

    public function prepareForValidation()
    {
        if (!$this->input('url')) {
            $this->merge([
                'url' => str_slug_new($this->input('title')),
            ]);
        }
        if (!$this->input('seoTitle')) {
            $this->merge([
                'seoTitle' => $this->input('title'),
            ]);
        }

        if ($this->input('created_at')) {
            $this->merge([
                'created_at' => Carbon::createFromFormat('d.m.Y H:i', $this->input('created_at'), 'Europe/Moscow')
            ]);
        }

        $news = News::find($this->input('id'));
        $imgArr = is_array($news->images) ? $news->images : json_decode($news->images);
        $currentImagePaths = array_map(function ($img) {
            return public_path($img);
        }, $imgArr);
        $images = $imgArr;
        if ($this->file('img')) {

            $product = News::find($this->input('id'));

            $imagesData = $this->file('img');


            $newImagePaths = [];

            foreach ($imagesData as $image) {
                $file = $image;
                $filePath = $file->getRealPath();
                $folderName = 'news';
                $fileName = $file->getClientOriginalName();
                $filePathSecond = public_path('storage/' . $folderName . '/' . $fileName);
                if (!in_array($filePathSecond, $currentImagePaths)) {
                    $imageObj = new \Imagick($filePath);
                    $imageObj->setImageFormat('webp');
                    $fileName = "/" . uniqid() . '.webp';
                    $disk = Storage::disk('public');

                    if (!$disk->exists($folderName)) {
                        $disk->makeDirectory($folderName);
                    }

                    $newFilePath = $disk->path($folderName) . $fileName;

                    $imageObj->writeImage($newFilePath);

                    $newImagePaths[] = "/storage/" . $folderName . $fileName;
                } else {
                    $newImagePaths[] = '/storage/' . $folderName . '/' . $fileName;
                }
            }

            foreach ($currentImagePaths as $path) {
                if (!in_array($path, array_map(function ($newPath) {
                    return public_path($newPath);
                }, $newImagePaths))) {
                    if (file_exists($path)) {
                        unlink($path);
                    }
                }
            }

            $this->merge([
                'images' => array_reverse($newImagePaths)
            ]);
        } else {
            foreach ($currentImagePaths as $path) {
                if (file_exists($path)) {
                    unlink($path);
                }
            }
            $this->merge([
                'images' => []
            ]);
        }
    }

    public function attributes(): array
    {
        return [
            'description' => 'описание',
            'content' => 'контент'
        ];
    }

    public function messages():array
    {
        return [
            'required' => "Нужно заполнить поле :attribute"
        ];
    }
}
