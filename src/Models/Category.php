<?php

namespace News\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $table = 'categories_news';

    protected $fillable = [
        'url','name'
    ];

    public function news(){
        return $this->belongsToMany(News::class, 'categories_has_news',
            'category_id', 'news_id', 'id', 'id');
    }
}
