<?php

namespace News\Http\Controllers;

use Illuminate\Support\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;
use News\Enums\NewsEnums;
use News\Models\News;
use News\QueryBuilder\NewsBuilder;
use News\Requests\CreateRequest;
use News\Requests\UpdateRequest;
use Illuminate\Contracts\Validation\Factory;
use News\Models\Settings;
use News\Models\Category;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(NewsBuilder $newsBuilder,Request $request)
    {
        return view('news::news.index',[
            'links' => $newsBuilder->getLinks(NewsEnums::NEWS->value)
        ]);
    }

    public function removeImage(News $news,Request $request)
    {

        try {
            $images = $news->images;
            $filteredImages = array_filter($images, function ($image) use ($request) {
                return $image !== $request->input('img');
            });
            $news->images = json_encode(array_values($filteredImages));
            if($news->save()){
                return ['status'=>true];
            }
        }catch (Exception $exception){
            return ['status'=>false];
        }
    }

      
    public function lock(News $news)
    {
        $news->update(['lock'=>true]);
        return redirect()->back()->with('success','Успешно');
    }
    public function unlock(News $news)
    {
        $news->update(['lock'=>false]);
        return redirect()->back()->with('success','Успешно');
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create(NewsBuilder $newsBuilder,Factory $factory)
    {
        return view('news::news.create',[
            'linksContent' => $newsBuilder->getLinksContent(NewsEnums::CONTENT->value),
            'links' => $newsBuilder->getLinks(NewsEnums::POST->value),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateRequest $request
     * @return RedirectResponse
     */
    public function store(CreateRequest $request): RedirectResponse
    {
        $news = News::create($request->validated());
        if ($news) {
            $news->categories()->attach($request->getCategoriesIds());
            return \redirect()->route('admin.news.create')->with('success', __('messages.admin.news.store.success'));
        }

        return \redirect()->route('admin.news.create')->with('error', __('messages.admin.news.store.fail'));
    }

    /**
     * Display the specified resource.
     */
    public function show(News $news)
    {
        $news->publish = !$news->publish;
        if ($news->save()) return ['status' => true, 'publish' => $news->publish];
        else  return ['status' => false];
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(News $news, NewsBuilder $newsBuilder)
    {
        return view('news::news.edit',[
            'linksContent' => $newsBuilder->getLinksContent(NewsEnums::CONTENT->value),
            'links' => $newsBuilder->getLinks(null),
            'news' => $news
        ]);
    }

    /**
     * Handle the incoming request.
     */
    public function frontIndex()
    {
        return \view('news::news.front.index',[
            'news'=>News::where('publish',true)->where('created_at', '<=', Carbon::now())
            ->orderByDesc('lock')
            ->orderBy('created_at', 'desc')->paginate(Settings::first()->paginate),
            'settings'=> Settings::query()->find(1),
        ]);
    }

    public function frontCategory(string $url)
    {
        $category = Category::where('url',$url)->first();
        return \view('news::news.front.category',[
            'news'=> $category?->news()->orderByDesc('created_at')->where('created_at', '<=', Carbon::now())->where('publish',true)->paginate(Settings::first()->paginate),
            'title' => $category->name,
            'settings'=> Settings::query()->find(1),
        ]);
    }

    public function news(string $url)
    {
        return view('news::news.front.news',['news'=>News::where('url',$url)->where('publish',true)->where('created_at', '<=', Carbon::now())->firstOrFail(),'settings'=> Settings::query()->find(1)]);
    }
    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, News $news)
    {
        $news = $news->fill($request->validated());
        if ($news->save()) {
            $news->categories()->sync($request->getCategoriesIds());
            return \redirect()->route('admin.news.edit',['news'=>$news])->with('success', __('messages.admin.news.update.success'));
        }

        return \back()->with('error', __('messages.admin.news.update.fail'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(News $news)
    {
        try {
            $news->delete();
            $response = ['status' => true,'message' => __('messages.admin.news.destroy.success')];
        } catch (Exception $exception)
        {
            $response = ['status' => false,'message' => NewsController . php__('messages.admin.news.destroy.fail') . $exception->getMessage()];
        }

        return $response;
    }
}
