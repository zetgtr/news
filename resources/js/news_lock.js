class NewsLock {
    constructor() {
        this.btnLock = document.querySelector('.btn-lock')
        this.btnUnlock = document.querySelector('.btn-unlock')
        this.addEvent()
    }

    lock(){
        axios.get('/news/lock/'+this.btnLock.dataset.id).then(res=>{
            if(res.data.status){
                this.btnLock.classList.remove('d-none')
                this.btnUnlock.classList.remove('d-none')
            }
        })
    }
    unlock(){
        axios.get('/news/unlock/'+this.btnLock.dataset.id).then(res=>{
            if(res.data.status){
                this.btnUnlock.classList.remove('d-none')
                this.btnLock.classList.remove('d-none')
            }
        })
    }

    addEvent(){
        this.btnLock.addEventListener('click',this.lock.bind(this))
        this.btnUnlock.addEventListener('click',this.unlock.bind(this))
    }
}
