@extends('layouts.inner')
@section('title', $title)
@section('description', $title)
@section('page', true)
@section('content')
    <div class="content">
        @php
            $breadcrumbs = [...config('news_module.routes'), ['title' => $title]];
        @endphp

        <div class="innerPage-header header_bredcrambs">
            <x-front.breadcrumbs :breadcrumbs="$breadcrumbs" flag='true' />
            <div class="container">
                <h1 class="wow fadeIn">{{ $title }}</h1>
            </div>
        </div>

        <x-news::front.category :news="$news" :settings="$settings" />
    </div>

@endsection
