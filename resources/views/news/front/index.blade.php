@extends('layouts.inner')
@section('title',$settings->seoTitle ?? $settings->title)
@section('description',$settings->seoDescription)
@section('keywords',$settings->seoKeywords)
@section('page',true)
@section('content')
<div class="content">
    {{-- <x-front.breadcrumbs :breadcrumbs="$breadcrumbs" /> --}}
    {{-- <h2>{{ $title }}</h2> --}}

    <x-news::front.category :news="$news" :settings="$settings"/>
</div>

@endsection
