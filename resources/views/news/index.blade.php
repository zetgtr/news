@extends('layouts.admin')
@section('title',config('news_module.title'))
@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <x-admin.navigation :links="$links" />
                <x-news::news />
            </div>
        </div>
    </div>
@endsection
@section('breadcrumb')
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route("admin.index")}}">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{route("admin.news.index")}}">{{config('news_module.title')}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Список</li>
        </ol>
    </div>
@endsection
