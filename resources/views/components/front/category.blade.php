{{-- @php
    $breadcrumbs = [...config('news_module.routes')];
@endphp
<div class="news__category">
    <div class="container">
        <x-front.breadcrumbs :breadcrumbs="$breadcrumbs"  flag='true'/>
        <h1>{{ $settings->title }}</h1>
        <div class="row news-item--row">
            @foreach ($news as $item)
                <div class="col-lg-4 col-sm-6 wow fadeIn">
                    <x-news::front.macro :item="$item" />
                </div>
            @endforeach
            <div>
                {{ $news->links() }}
            </div>
        </div>
    </div>
</div> --}}
{{-- @php
    $breadcrumbs = [...config('news_module.routes')];
@endphp --}}

<div class="innerPage-header header_bredcrambs">
    {{-- <x-front.breadcrumbs :breadcrumbs="$breadcrumbs" flag='true' />
    <div class="container">
        <h1 class="wow fadeIn">{{ $settings->title }}</h1>
    </div> --}}
</div>
<section class="news-inner">
    <div class="container">
        <div class="row news-item--row">
            @foreach ($news as $item)
                <div class="col-lg-4 col-sm-6 wow fadeIn">
                    <x-news::front.macro :item="$item" />
                </div>
            @endforeach
            <div>
                {{-- {{ $news->onEachSide(1)->links() }} --}}

            </div>
        </div>
    </div>
</section>
{{-- @vite("resources/js/pagination/pagination.js") --}}
