{{-- @dd($news) --}}
@php
use Carbon\Carbon;
$formattedDate = Carbon::parse($news->created_at)
->locale('ru')
->isoFormat('D MMMM YYYY');

$news->setCountShow();
$breadcrumbs = [...config('news_module.routes'), ['title' => $news->title, 'route' => 'news']];
@endphp



<div class="innerPage-header header_bredcrambs">
    <x-front.breadcrumbs :breadcrumbs="$breadcrumbs" flag='true' />
    <div class="container">
        <h1 class="wow fadeIn">{!! $news->title !!}</h1>
    </div>
</div>
<section class="news-inner">
    <div class="container news-container">
        <div class="main">
            <div class="news__header__item text-small text-gloom position-relative container__text wow fadeIn">
                <div class="news__header-item">
                    <div> <i class="far fa-clock"></i>
                        {{ $formattedDate }}</div>

                    <div>·</div>
                    <div class="news-item__category">
                        {{ $news->categories[0]->name }}
                    </div>

                </div>
                <div class="news-item__info">
                    <div class="news-item__eye">
                        <i class="far fa-eye"></i>
                        {{ $news->show }}
                    </div>
                </div>
                @auth
                <a target="_blank" class="news-item__edit" href="{{ route('admin.news.edit', [
                            'news' => $news,
                        ]) }}">
                    <i class="far fa-edit"></i>
                    <span>Редактировать</span>
                </a>
                @endauth
            </div>

            <div class="news__content news-text">
                <div class="container__text wow fadeIn">
                    {!! $news->content !!}
                </div>
                @if ($news->images !== '[null]')
                <div class="news-slider-container wow fadeIn">
                    <div class="news-slider swiper">
                        <div class="swiper-wrapper">
                            @foreach ($news->images as $image)
                            <div class="swiper-slide">
                                <div class="item">
                                    <div class="img-container">
                                        <img src="{{ $image }}" alt="{{ $news->title }}">
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
                @endif
                @if ($news->getOther())
                <div class="other-news wow fadeIn">
                    <div class="other-news__header">
                        <h2>Другие новости</h2>
                        <div class="other-news__btns">
                            <div class="other-news__prev">
                                <svg width="29" height="51" viewBox="0 0 29 51" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M27.9789 1.53627C29.257 2.81439 29.257 4.85938 27.9789 6.08637L8.55149 25.5138L27.9789 44.9412C29.257 46.2193 29.257 48.2643 27.9789 49.4913C27.3654 50.1048 26.5474 50.4626 25.6783 50.4626C24.8092 50.4626 23.9912 50.1048 23.3777 49.4913L1.64965 27.7633C1.03615 27.1498 0.678283 26.3318 0.678283 25.4626C0.678283 24.5935 1.03615 23.7755 1.64965 23.162L23.3777 1.43401C23.9912 0.820518 24.8092 0.462648 25.6783 0.462648C26.5985 0.564897 27.3654 0.92277 27.9789 1.53627Z" fill="black"></path>
                                </svg>
                            </div>
                            <div class="other-news__next">
                                <svg width="29" height="51" viewBox="0 0 29 51" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1.63687 1.53627C0.358754 2.81439 0.358754 4.85938 1.63687 6.08637L21.0643 25.5138L1.63687 44.9412C0.358754 46.2193 0.358754 48.2643 1.63687 49.4913C2.25037 50.1048 3.06837 50.4626 3.93749 50.4626C4.80661 50.4626 5.6246 50.1048 6.2381 49.4913L27.9661 27.7633C28.5796 27.1498 28.9375 26.3318 28.9375 25.4626C28.9375 24.5935 28.5796 23.7755 27.9661 23.162L6.2381 1.43401C5.6246 0.820518 4.80661 0.462648 3.93749 0.462648C3.01724 0.564897 2.25037 0.92277 1.63687 1.53627Z" fill="black"></path>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div class="swiper other-news__swiper">
                        <div class="swiper-wrapper">
                            @foreach ($news->getOther() as $item)
                            <div class="swiper-slide">
                                <div class="item">
                                    <x-news::front.macro :item="$item" flag='true' />
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                @endif


                <div class="news__footer-item wow fadeIn">
                    <a href="{{ route('news') }}" class="news-footer-item-back">
                        <i class="fas fa-chevron-left"></i>
                        <span>Назад</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

@vite('resources/js/news-inner/news-slider.js')
