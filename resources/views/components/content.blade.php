<div class="tab-pane active" id="content" role="tabpanel">
    @if(!empty($news))
        @dd($news)
    @endif
    <div class="row">
        <div class="col-lg-7">
            <div class="form-grop mb-3">
                <label for="title">Заголовок</label>
                <input id="title" class="form-control @error('title') is-invalid @enderror" name="title" value="{{old('title')}}@if(!empty($news)) {{ $news->title }} @endif" />
                <x-error error-value="title" />
            </div>
            <div class="form-group mb-3">
                <label for="my-editor" >Описание</label>
                <textarea name="description" id="my-editor" class="form-control @error('description') is-invalid @enderror my-editor">{{ old('description') }}@if(!empty($news)) {{ $news->description }} @endif</textarea>
                <x-error error-value="description" />
            </div>
            <div class="form-group mb-3">
                <label for="content" >Контент</label>
                <textarea name="content" id="my-editor" class="form-control @error('content') is-invalid @enderror my-editor">{{ old('content') }}</textarea>
                <x-error error-value="content" />
            </div>
        </div>
        <div class="col-lg-5">
            <div class="form-group">
                <label for="category">Категория</label>
                <select name="category_id" id="category" class="form-select">
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group" style="position:relative;">
                <label for="date_news">Дата публикации (необязательно):</label>
                <input type="text" data-language="ru" name="created_at" id="addDates" class="form-control" value="{{old('created_at')}}">
            </div>
            <div class="form-group ">
                <label for="">Изображения</label>
                <input type="file" class="filepond " name="img[]" multiple>
                <x-error error-value="img" />
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('assets/plugins/gallery/lightgallery.js') }}"></script>
<script src="{{ asset('assets/plugins/gallery/lightgallery-1.js') }}"></script>

@vite('resources/js/utils/AirDatepicker.js')
