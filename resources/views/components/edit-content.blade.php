<style>
    .img_box{
        position: relative;
    }
    .remove{
        position: absolute;
        right: 15px;
        cursor: pointer;
        font-size: 16px;
    }
    .remove:hover{
        color: red;
    }
</style>
<div class="tab-pane active" id="content" role="tabpanel">
    <input type="hidden" name="id" value="{{ $news->id }}" />
    <div class="row">
        <div class="col-lg-7">
            <div class="form-grop mb-3">
                <label for="title">Заголовок</label>
                <input id="title" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') ? old('title') : $news->title }}" />
                <x-error error-value="title" />
            </div>
            <div class="form-group mb-3">
                <label for="my-editor" >Описание</label>
                <textarea name="description" id="my-editor" class="form-control @error('description') is-invalid @enderror my-editor">{{ old('description') ? old('description') : $news->description }}</textarea>
                <x-error error-value="description" />
            </div>
            <div class="form-group mb-3">
                <label for="content" >Контент</label>
                <textarea name="content" id="my-editor" class="form-control @error('content') is-invalid @enderror my-editor">{{ old('content') ? old('content') : $news->content }}</textarea>
                <x-error error-value="content" />
            </div>
        </div>
        <div class="col-lg-5">
            <div class="form-group">
                <label for="category">Категория</label>
                <select name="category_id" id="category" class="form-select">
                    @foreach($categories as $category)
                        <option @if($categoryNews === $category->id) selected @endif value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group" style="position:relative;">
                <label for="date_news">Дата публикации (необязательно):</label>
                <input type="text" data-language="ru" name="created_at" id="addDates" class="form-control @error('created_at') is-invalid @enderror" value="{{ old('created_at') ? old('created_at') : $news->created_at->format('d.m.Y H:i') }}">
            </div>
            <div class="form-group ">
                <label for="">Изображения</label>
                <input type="file" class="filepond " name="img[]" multiple>
                <x-error error-value="img" />
                <input type="hidden" value="{{ json_encode($news->images) }}" id="images_filepond">
            </div>
        </div>
    </div>
</div>


@vite('resources/js/utils/AirDatepicker.js')
